# The factorial of a number n is defined as n! = n * (n - 1) * (n - 2) * ... * 1.
#
# Given a positive integer a, return n such that n! = a. If there is no integer n that is a factorial, then return -1.
#
# Constraints
#
#     0 < a < 2 ** 31
#
# found @ https://binarysearch.com/problems/Inverse-Factorial

#       if 5! = 5*4*3*2*1 = 120
#       a = 120 then what is n?
#         120/1 = 120
#         120/2 = 60
#         60/3 = 20
#         20/4 = 5
#         5/5 = 1 <---- solve case when division returns 1.  if it doesnt return 1 then we cant solve it?

#       a = 10
#           10/1 = 10
#           10/2 = 5
#           5/3 = <--- not an integer so we fail return -1

#       a = 6
#           6/1 = 6
#           6/2 = 3
#           3/3 = 1

#       dividend / divisor = quotient

class InverseFactorial:

    def __init__(self, a):
        self.a = a

    def solve(self):

        dividend = self.a
        divisor = 1
        quotient = 0
        self.a = -1

        while quotient % 1 == 0 and quotient != 1:
            quotient = dividend / divisor
            divisor = divisor + 1
            dividend = quotient
            if quotient == 1:
                self.a = divisor - 1
