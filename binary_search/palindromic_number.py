# Given a non-negative integer num, return whether it is a palindrome.
# Bonus: Can you solve it without using strings?
# Constraints
#     num < 2 ** 31
# Example 1
# Input
# num = 121
# Output
# True
# Example 2
# Input
# num = 20200202
# Output
# True
# Example 3
# Input
# num = 43
# Output
# False

class PalindromicNumber:
    def __init__(self, num):
        self.num = num

    def solve(self):
        num = self.num
        num_orig = num
        num_backwards = 0
        while num != 0:
            num_backwards = (num_backwards * 10) + (num % 10)
            num /= 10
            num = int(num)
        return num_backwards == num_orig
