# Minimum Bracket Addition
#
# Easy
#
# Given a string s containing brackets ( and ), return the minimum number of brackets that can be inserted so that
# the brackets are balanced.
#
# Constraints
#
#     n ≤ 100,000 where n is the length of s
#
#   https://binarysearch.com/problems/Minimum-Bracket-Addition

class MinimumBracketAddition:
    def __init__(self, s):
        self.s = s

    def solve(self):
        s = self.s
        stack = []
        needed = 0

        for char in s:
            if char == '(':
                stack.append(char)
            if char == ')':
                if len(stack) == 0:
                    needed = needed + 1
                elif stack[-1] == '(':
                    stack.pop()
                else:
                    stack.insert(0, '(')
                    needed = needed + 1

        return needed + len(stack)








