# Split List
#
# Easy
#
# Given a list of integers nums, return whether you can partition the list into two non-empty sublists
# such that every number in the left sublist is strictly less than every number in the right sublist.
#
# Constraints
#
#     n ≤ 100,000 where n is the length of nums.
# https://binarysearch.com/problems/Split-List

class SplitList:
    def __init__(self, nums):
        self.nums = nums

    def solve(self):
        nums = self.nums
        len_nums = len(nums)

        if len(nums) <= 1:
            return False

        maximum = left_max = nums[0]
        left_size = 1
        for i in range(len_nums):
            maximum = max(nums[i], maximum)
            if nums[i] <= left_max:
                left_size = i + 1
                left_max = maximum
        return left_size < len_nums
























        #
        #     left_nums = nums[:i+1]
        #     right_nums = nums[i+1:]
        #     if left_nums and right_nums:
        #         if max(left_nums) < min(right_nums):
        #             return True
        # return False
