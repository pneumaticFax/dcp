from collections import Counter


class AnagramChecks:

    def __init__(self, s0, s1):
        self.s0 = s0
        self.s1 = s1

    def solve(self):
        s0 = self.s0
        s1 = self.s1
        the_same = True
        ordered_s0 = set()
        ordered_s1 = set()
        diff_set = set()
        sum_s0 = 0
        sum_s1 = 0
        if len(s0) != len(s1):
            return False
        for char in s0:
            sum_s0 = sum_s0 + ord(char)
            ordered_s0.add(ord(char))
        for char in s1:
            sum_s1 = sum_s1 + ord(char)
            ordered_s1.add(ord(char))
        if ordered_s0 != ordered_s1 or sum_s1 != sum_s0:
            return False
        return the_same

    def solve2(self):
        s0 = self.s0
        s1 = self.s1
        return Counter(s0) == Counter(s1)

