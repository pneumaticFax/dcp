import unittest
from binary_search import detect_the_only_duplicate_in_a_list


class MyTestCase(unittest.TestCase):

    test_case = detect_the_only_duplicate_in_a_list.DetectTheOnlyDuplicateInAList([4, 2, 1, 3, 2])
    test_case1 = detect_the_only_duplicate_in_a_list.DetectTheOnlyDuplicateInAList([1, 2, 3, 1])

    def test_something(self):
        self.assertEqual(2, self.test_case.solve())  # add assertion here

    def test_something1(self):
        self.assertEqual(1, self.test_case1.solve())  # add assertion here


if __name__ == '__main__':
    unittest.main()
