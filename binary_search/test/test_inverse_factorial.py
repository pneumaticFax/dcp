import unittest
from binary_search import inverse_factorial


class MyTestCase(unittest.TestCase):
    def test_something(self, test_value=6):
        test = inverse_factorial.InverseFactorial(test_value)
        test.solve()
        self.assertEqual(3, test.a)  # add assertion here


if __name__ == '__main__':
    unittest.main()
