import unittest
from binary_search import anagram_check


class MyTestCase(unittest.TestCase):
    def test_something(self):
        s0 = "kjarxquppfm"
        s1 = "ajukrmrqpqk"
        test_case = anagram_check.AnagramChecks(s0, s1)
        # self.assertFalse(test_case.solve())  # add assertion here
        self.assertFalse(test_case.solve2())


if __name__ == '__main__':
    unittest.main()
