import unittest
from binary_search import monotonous_string_groups


class MyTestCase(unittest.TestCase):
    test_case = monotonous_string_groups.MonotonousStringGroups("abcdcba")
    test_case2 = monotonous_string_groups.MonotonousStringGroups("bca")
    test_case3 = monotonous_string_groups.MonotonousStringGroups("bcb")
    test_case4 = monotonous_string_groups.MonotonousStringGroups("cab")
    test_case5 = monotonous_string_groups.MonotonousStringGroups("dcda")
    test_case6 = monotonous_string_groups.MonotonousStringGroups("abzy")

    def test_something(self):
        self.assertEqual(2, self.test_case.solve())  # add assertion here

    def test_something2(self):
        self.assertEqual(2, self.test_case2.solve())  # add assertion here

    def test_something3(self):
        self.assertEqual(2, self.test_case3.solve())  # add assertion here

    def test_something4(self):
        self.assertEqual(2, self.test_case4.solve())  # add assertion here

    def test_something5(self):
        self.assertEqual(2, self.test_case5.solve())  # add assertion here

    def test_something6(self):
        self.assertEqual(2, self.test_case6.solve())  # add assertion here

if __name__ == '__main__':
    unittest.main()
