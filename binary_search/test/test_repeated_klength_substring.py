import unittest
from binary_search import repeated_klength_substring


class MyTestCase(unittest.TestCase):
    def test_something(self, test_string="abcdabc", test_length=3):
        repeated = repeated_klength_substring.RepeatedKLengthSubstrings(test_string, test_length)
        repeated.repeated_klength_substrings()
        self.assertEqual(1, repeated.counter)  # add assertion here


if __name__ == '__main__':
    unittest.main()
