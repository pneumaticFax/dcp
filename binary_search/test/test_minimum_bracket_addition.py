import unittest
from binary_search import minimum_bracket_addition


class MyTestCase(unittest.TestCase):
    def test_something1(self):
        test_case = minimum_bracket_addition.MinimumBracketAddition(")))((")
        self.assertEqual(5, test_case.solve())  # add assertion here
    def test_something2(self):
        test_case = minimum_bracket_addition.MinimumBracketAddition(")))(()))((")
        self.assertEqual(6, test_case.solve())  # add assertion here

if __name__ == '__main__':
    unittest.main()
