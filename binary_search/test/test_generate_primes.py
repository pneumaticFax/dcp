import unittest
from binary_search import generate_primes


class MyTestCase(unittest.TestCase):
    test_case = generate_primes.GeneratePrimes(10)
    test_case2 = generate_primes.GeneratePrimes(17)
    test_case3= generate_primes.GeneratePrimes(3)
    test_case4= generate_primes.GeneratePrimes(100)

    def test_something(self):
        self.assertEqual([2, 3, 5, 7], self.test_case.solve())  # add assertion here

    def test_something2(self):
        self.assertEqual([17, 13, 11, 7, 5, 3, 2, 1], self.test_case2.solve())  # add assertion here

    def test_something3(self):
        self.assertEqual([2,3], self.test_case3.solve())  # add assertion here

    def test_something4(self):
        self.assertEqual([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97], self.test_case4.solve())  # add assertion here


if __name__ == '__main__':
    unittest.main()

