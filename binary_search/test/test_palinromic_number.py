import unittest
from binary_search import palindromic_number


class MyTestCase(unittest.TestCase):
    test_case = palindromic_number.PalindromicNumber(121)

    def test_something(self):
        self.assertTrue(self.test_case.solve())  # add assertion here



if __name__ == '__main__':
    unittest.main()
