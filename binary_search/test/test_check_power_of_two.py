import unittest
from binary_search import check_power_of_two


class MyTestCase(unittest.TestCase):
    test_case0 = check_power_of_two.CheckPowerOfTwo(0)
    test_case1 = check_power_of_two.CheckPowerOfTwo(1)
    test_case2 = check_power_of_two.CheckPowerOfTwo(2)
    test_case3 = check_power_of_two.CheckPowerOfTwo(5)
    test_case4 = check_power_of_two.CheckPowerOfTwo(32)


    def test_something0(self):
        self.assertFalse(self.test_case0.solve2())  # add assertion here

    def test_something1(self):
        self.assertTrue(self.test_case1.solve2())  # add assertion here

    def test_something2(self):
        self.assertTrue(self.test_case2.solve2())  # add assertion here

    def test_something3(self):
        self.assertFalse(self.test_case3.solve2())  # add assertion here

    def test_something4(self):
        self.assertTrue(self.test_case4.solve2())  # add assertion here


if __name__ == '__main__':
    unittest.main()
