import unittest
from binary_search import set


class MyTestCase(unittest.TestCase):
    def test_something(self):
        new_set = set.CustomSet()
        new_set.add(1)
        self.assertTrue(new_set.exists(1))  # add assertion here
        new_set.remove(1)
        self.assertFalse(new_set.exists(1))  # add assertion here

    def test_something2(self):
        new_set = set.CustomSet()
        new_set.add('abc')
        new_set.add('def')
        new_set.add('hij')
        new_set.add('klmn')

        self.assertTrue(new_set.exists('abc'))  # add assertion here
        new_set.remove('abc')
        self.assertFalse(new_set.exists('abc'))  # add assertion here


if __name__ == '__main__':
    unittest.main()
