import unittest
from binary_search import split_list


class MyTestCase(unittest.TestCase):
    def test_something1(self):
        test_case = split_list.SplitList([5, 3, 2, 7, 9])
        self.assertTrue(test_case.solve())  # add assertion here

    def test_something2(self):
        test_case = split_list.SplitList([1, 0])
        self.assertFalse(test_case.solve())  # add assertion here

    def test_something3(self):
        test_case = split_list.SplitList([0, 0, 1])
        self.assertTrue(test_case.solve())  # add assertion here]

    def test_something4(self):
        test_case = split_list.SplitList([0, 0])
        self.assertFalse(test_case.solve())  # add assertion here]

    def test_something5(self):
        test_case = split_list.SplitList([5, 3, 2, 4, 7, 9])
        self.assertTrue(test_case.solve())  # add assertion here


if __name__ == '__main__':
    unittest.main()
