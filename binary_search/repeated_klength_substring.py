# Given a string s and an integer k, return the number of k-length substrings that occur more than once in s.
#
# Constraints
#     n ≤ 100,000 where n is the length of s.
#     k ≤ 10
#
# Problem found @ https://binarysearch.com/problems/Repeated-K-Length-Substrings

class RepeatedKLengthSubstrings:

    def __init__(self, input_string, input_length):
        self.s = input_string
        self.k = input_length
        self.counter = 0

    def repeated_klength_substrings(self):

        substring_dict = {}
        string = self.s
        k = self.k

        for index, letter in enumerate(string):
            if index + k < len(string)+1:
                substring = string[index: index + k]
                if substring not in substring_dict.keys():
                    substring_dict.update({substring: 1})
                elif substring in substring_dict.keys():
                    substring_dict[substring] = substring_dict[substring] + 1

        for value in substring_dict.values():
            if value > 1:
                self.counter = self.counter + 1
