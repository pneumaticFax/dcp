# Given a number n, return a list of all prime numbers smaller than or equal to n in ascending order.
# Constraints
#     n ≤ 100,000
# Example 1
# Input
# n = 3
# Output
# [2, 3]
# Example 2
# Input
# n = 10
# Output
# [2, 3, 5, 7]
# Example 3
# Input
# n = 20
# Output
# [2, 3, 5, 7, 11, 13, 17, 19]
# https://binarysearch.com/problems/Generate-Primes
import math


class GeneratePrimes:
    def __init__(self, n):
        self.n = n

    def solve(self):
        n = self.n
        primes = []
        if n == 0 or n == 2 or n == 1 or n == 3:
            if n == 3:
                primes.append(2)
                primes.append(3)
            if n == 2:
                primes.append(2)
            return primes

        test = 4
        primes.append(2)
        primes.append(3)
        while test <= n:
            test1 = (test + 1) / 6
            test2 = (test - 1) / 6
            if test1 % 1 == 0 or test2 % 1 == 0:
                if (test/5) % 1 != 0:
                    primes.append(test)
            test += 1
        return primes
