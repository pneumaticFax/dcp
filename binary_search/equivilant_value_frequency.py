# Equivalent Value and Frequency
# Given a list of integers nums, return whether there's an integer whose frequency in the list is same as its value.
#
# Constraints
#
#     n ≤ 100,000 where n is the length of nums
# https://binarysearch.com/problems/Equivalent-Value-and-Frequency



from collections import Counter


class EquivilantValueFrequency:

    def __init__(self, nums):
        self.nums = nums

    def solve(self):
        nums = self.nums
        counter_nums = Counter(nums)
        for key in counter_nums:
            value = counter_nums[key]
            if value == key:
                return True
        return False




