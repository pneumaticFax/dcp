# Largest One Submatrix with Column Swaps
#
# Medium
#
# You are given a two-dimensional list of integers matrix containing 1s and 0s. Given that you can first rearrange the columns as many times as you want, return the area of the largest submatrix containing all 1s.
#
# Constraints
#
#     1 ≤ n * m ≤ 100,000 where n and m are the number of rows and columns in matrix
#

class LargestOneSubmatrixWithColumnSwaps:
    def __init__(self, matrix):
        self.matrix = matrix

    def solve(self):
        matrix = self.matrix

        for i in range(len(matrix)):
            count = 0
            column_list = {}
            for j in reversed(range(len(matrix[j]))):
                if matrix[i][j] == 1:
                    count = count + 1

            column_list.update({i, count})

