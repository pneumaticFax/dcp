# Given a list of integers nums, return the maximum length of a contiguous strictly
# increasing sublist if you can remove one or zero elements from the list.
#
# Constraints
#     n ≤ 100,000 where n is the length of nums
#
# Input
# nums = [30, 1, 2, 3, 4, 5, 8, 7, 22]
#
# Output
# 7
#
# Explanation
# If you remove 8 in the list you can get [1, 2, 3, 4, 5, 7, 22]
# which is the longest, contiguous, strictly increasing list.

class LongestContiguouslyStrict:

    def __init__(self, nums):
        self.nums = nums

    def solve(self):
        # get start index
        old_list = self.nums
        sorted_list = old_list.copy()
        sorted_list.sort()

        new_list = []
        removed = 0

        for index, element in enumerate(old_list):
            if old_list.next(index) > element:
                new_list.append(element)
            elif element.next() <= element and removed == 0:
                if index != 0:
                    removed = 1
                    new_list.append(element.next())

        print(len(new_list))
