# Given an integer n greater than or equal to 0, return whether it is a power of two.
#
# Constraints
#
#     0 ≤ n < 2 ** 31
#
class CheckPowerOfTwo:
    def __init__(self, n):
        self.n = n

    def solve(self):
        n = self.n
        bin_n = bin(n)
        bin_n = bin_n[2::]
        counter = 0
        for char in bin_n:
            if char == '1':
                counter = counter + 1
        if counter == 1:
            return True
        return False

    def solve2(self):
        n = self.n

        if n == 0:  # 0 isn't a power of 2 technically
            return False
        return (n & -n) == n
