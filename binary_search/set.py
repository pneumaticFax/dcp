# Set
#
# Easy
#
# Implement a set data structure with the following methods:
#
#     CustomSet() constructs a new instance of a set
#     add(int val) adds val to the set
#     exists(int val) returns whether val exists in the set
#     remove(int val) removes the val in the set
#
# This should be implemented without using built-in set.
#
# Constraints
#
#     n ≤ 100,000 where n is the number of calls to add, exists and remove
#
# Example 1

class CustomSet:
    def __init__(self):
        self.table = {}

    def add(self, val):
        key = hash(val)
        self.table.update({key: val})

    def exists(self, val):
        if self.table is None:
            return False
        else:
            key = hash(val)
            return self.table.get(key) is not None
        # return False

    def remove(self, val):
        key = hash(val)
        if self.exists(val):
            del self.table[key]


