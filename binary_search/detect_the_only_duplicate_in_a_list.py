# Detect the Only Duplicate in a List
#
# Easy
#
# You are given a list nums of length n + 1 picked from the range 1, 2, ..., n. By the pigeonhole principle,
# there must be a duplicate. Find and return it. There is guaranteed to be exactly one duplicate.
#
# Bonus: Can you do this in O(n)\mathcal{O}(n)O(n) time and O(1)\mathcal{O}(1)O(1) space?
#
# Constraints
#
#     n ≤ 10,000
#
# Example 1
# Input
#
# nums = [1, 2, 3, 1]
#
# Output
#
# 1
#
# Example 2
# Input
#
# nums = [4, 2, 1, 3, 2]
#
# Output
#
# 2
# https://binarysearch.com/problems/Detect-the-Only-Duplicate-in-a-List

class DetectTheOnlyDuplicateInAList:
    def __init__(self, nums):
        self.nums = nums

    def solve(self):
        nums = self.nums
        # plan traverse in place
        count = 0
        length = len(nums)
        sum_nums = (length - 1) * length/2

        for i in range(length):
            count = count + nums[i]

        return count - sum_nums









