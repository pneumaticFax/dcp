# \Linked List Union
#
# Medium
#
# Given two sorted linked lists node0, and node, return a new sorted linked list containing the union of the two lists.
#
# Constraints
#
#     0 ≤ n ≤ 100,000 where n is the number of nodes in node0
#     0 ≤ m ≤ 100,000 where m is the number of nodes in node1
# https://binarysearch.com/problems/Linked-List-Union

class LLNode:
    def __init__(self, val, next_node=None):
        self.val = val
        self.next_node = next_node


class LinkedList:

    def __init__(self, ll0, ll1):
        self.ll0 = ll0
        self.ll1 = ll1

    def solve(self):     # TODO: fix this one

        ll0 = self.ll0
        ll1 = self.ll1
        combined = set()
        #
        # head = LLNode(None)
        # current_node = head
        #
        # while ll1 and ll0:
        #     # if ll1.val == ll0.val
        #     #     current_node.next = LLNode(ll0.val)
        #     #     ll0 = ll0.next
        # for element in ll0:
        #     combined.add(element)
        # for element in ll1:
        #     combined.add(element)
        new_ll = sorted(combined)

        return new_ll

