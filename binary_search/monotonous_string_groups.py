# You are given a lowercase alphabet string s. Return the minimum numbers of contiguous substrings in
# which s must be broken into such that each substring is either non-increasing or non-decreasing.
# For example, "acccf" is a non-decreasing string, and "bbba" is a non-increasing string.
# Constraints
#     n ≤ 100,000 where n is the length of s
# Example 1
# Input
# s = "abcdcba"
# Output
# 2
# Explanation
# We can break s into "abcd" + "cba"
# Example 2
# Input
# s = "zzz"
# Output
# 1
# Explanation
# We can break s into just "zzz"

class MonotonousStringGroups:
    def __init__(self, s):
        self.s = s

    def solve(self):
        s = self.s
        if len(s) == 0:
            return 0
        if len(s) == 1 or len(s) == 2:
            return 1
        # ord_s = ord(s) # convert s into a array of values
        ord_s = []
        for i in range(len(s)):
            ord_s.append(ord(s[i]))

        increasing = False
        decreasing = False
        index_of_change = []
        counter_of_change = 1
        skip_cycle = False
        if len(s) == 0:
            return 0
        last_val = ord_s[0]

        for i in range(1, len(ord_s)):
            if increasing is False and decreasing is False:
                if ord_s[i] > ord_s[i-1]:
                    decreasing = False
                    increasing = True
                if ord_s[i] < ord_s[i-1]:
                    decreasing = True
                    increasing = False

            else:
                if increasing and ord_s[i] < ord_s[i-1]:
                    decreasing = False
                    increasing = False
                    index_of_change.append(i)
                    counter_of_change = counter_of_change + 1

                if decreasing and ord_s[i] > ord_s[i-1]:
                    decreasing = False
                    increasing = False
                    index_of_change.append(i)
                    counter_of_change = counter_of_change + 1

        print(s)
        print(ord_s)
        print(index_of_change)
        print(counter_of_change)
        return counter_of_change
