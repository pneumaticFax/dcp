
class Dcp12:

    def __init__(self, array):
        self.array = array
        self.window = {None, None}

    def calculate_smallest_sorting_window(self):
        # calculate the smallest window needed to sort an array
        # e.g. [1,5,3,7,6] should be (1,4)
        left, right = None, None
        min_seen = float("inf")
        max_seen = float("-inf")

        for i in range(len(self.array)):
            max_seen = max(max_seen, self.array[i])
            if self.array[i] < max_seen:
                right = i

        for i in range(len(self.array) -1, -1, -1):
            min_seen = min(min_seen, self.array[i])
            if self.array[i] > min_seen:
                left = i

        self.window = {left, right}
        return self.window
