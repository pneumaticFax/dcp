# calculate the maximum subarray sum
# given an array of [34, -50, 42, 14, -5, 86]
# calculate the maximum subarray eg 42, 14, -5, 86

class Dcp13:

    def __init__(self, array):
        self.array = array
        self.window = (None, None)

    def calculate_maximum_subarray_greedy(self):
        # done in O(n^3) - three nested loops
        max_current = float("-inf")
        left = 0
        right = 0
        for i in range(len(self.array)):
            for j in range(i, len(self.array)):
                current_sum = sum(self.array[i:j + 1])
                if current_sum > max_current:
                    max_current = current_sum
                    left = i
                    right = j
                else:
                    max_current = max_current

                self.window = (left, right)
        return max_current

    def calculate_maximum_subarray_better(self):
        # using kadanes algorithm this is in O(n)
        maximum_sum = float("-inf")
        current_sum = 0
        right = 0
        left = 0
        # iterate over the array summing each element to current sum and check
        # if current sum is > maximum sum update max sum
        # if current sum is < 0 make current sum = to 0
        for i in range(len(self.array)):
            current_sum += self.array[i]
            if current_sum > maximum_sum:
                maximum_sum = current_sum
                right = i
            if current_sum < 0:
                current_sum = 0
                left = i+1
                # right = i+1
        self.window = (left, right)
        return maximum_sum
