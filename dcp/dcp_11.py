def calculate_product(input_array):
    # Given an input array create a new array with each element being
    # the product of the entire array except the value
    # at index i
    # :input_array

    # initially compute product for all elements of array
    product = 1
    for element in input_array:
        product *= element
    output_array = []
    for element in input_array:
        output_array.append((int)(product / element))
    return output_array


def calculate_product_no_division(input_array):
    # Given an input array create a new array with each element being
    # the product of the entire array except the value
    # at index i, don't use division
    output_array = []
    prefix_array = []
    suffix_array = []

    for element in input_array:
        if prefix_array:
            prefix_array.append(prefix_array[-1] * element)
        else:
            prefix_array.append(element)

    for element in reversed(input_array):
        if suffix_array:
            suffix_array.append(suffix_array[-1] * element)
        else:
            suffix_array.append(element)
    suffix_array = list(reversed(suffix_array))

    for i in range(len(input_array)):
        if i == 0:
            output_array.append(suffix_array[i + 1])
        elif i == len(input_array) - 1:
            output_array.append(prefix_array[i - 1])
        else:
            output_array.append(prefix_array[i - 1] * suffix_array[i + 1])
    return output_array
