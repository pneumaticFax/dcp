import numpy as np
from unittest import TestCase
from dcp.dcp_13 import Dcp13


class TestDcp13(TestCase):
    def test_calculate_maximum_subarray_greedy(self):
        test_array = [34, -50, 42, 14, -5, 86]
        dcp13 = Dcp13(test_array)
        maximum_sum = dcp13.calculate_maximum_subarray_greedy()
        # print(dcp13.max_current)
        self.assertTrue(maximum_sum == 137, "Base Test Case")

    def test_calculate_maximum_subarray_better(self):
        test_array = [34, -50, 42, 14, -5, 86]
        dcp13 = Dcp13(test_array)
        maximum_sum = dcp13.calculate_maximum_subarray_better()
        # print(dcp13.max_current)
        self.assertTrue(maximum_sum == 137, "Better Test Case")

    def test_calculate_maximum_subarray(self):
        test_array = np.random.randint(-101, 101, np.random.randint(10))
        print(test_array)
        dcp13 = Dcp13(test_array)
        # print([ 80, -87,  72])
        # dcp13 = Dcp13([ 80, -87,  72])

        maximum_sum1 = dcp13.calculate_maximum_subarray_greedy()
        print(dcp13.window)
        print(maximum_sum1)
        maximum_sum2 = dcp13.calculate_maximum_subarray_better()
        print(dcp13.window)
        print(maximum_sum2)

        self.assertTrue(maximum_sum1 == maximum_sum2, "compare Test Case")
