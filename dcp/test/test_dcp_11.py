from unittest import TestCase
from dcp import dcp_11

import numpy as np


class Test(TestCase):

    def test_calculate_product(self):
        test_array = np.random.randint(1, 11, np.random.randint(10))
        print("array to test: ", test_array)
        output_array = dcp_11.calculate_product(test_array)
        print("output array:", output_array)
        self.assertTrue(True, "Asserting True is True")

    def test_calculate_product_no_division(self):
        test_array = np.random.randint(1, 6, np.random.randint(10))

        print("1: array to test: ", test_array)
        output_array1 = dcp_11.calculate_product(test_array)
        print("1: output array:", output_array1)

        print("2: array to test: ", test_array)
        output_array2 = dcp_11.calculate_product_no_division(test_array)
        print("2: output array:", output_array2)
        self.assertTrue(output_array2 == output_array1, "Failed: output arrays differ")

        # self.fail('test failed')
