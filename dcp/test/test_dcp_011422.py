# Good morning! Here's your coding interview problem for today.
# This problem was asked by Facebook.
# Given a string of round, curly, and square open and closing brackets,
# return whether the brackets are balanced (well-formed).
# For example, given the string "([])[]({})", you should return true.
# Given the string "([)]" or "((()", you should return false.

from dcp.dcp_011422 import Dcp011422
from unittest import TestCase

class TestDcp13(TestCase):

    def test_0(self):
        test_string = "([])[]({})"
        dcp011422 = Dcp011422(test_string)
        self.assertTrue(dcp011422.determine_string_balance(), "test0")

    def test_1(self):
        test_string = "((()"
        dcp011422 = Dcp011422(test_string)
        self.assertFalse(dcp011422.determine_string_balance(), "test1")

    def test_2(self):
        test_string = "([)]"
        dcp011422 = Dcp011422(test_string)
        self.assertFalse(dcp011422.determine_string_balance(), "test2")

    def test_2(self):
        test_string = ""
        dcp011422 = Dcp011422(test_string)
        self.assertFalse(dcp011422.determine_string_balance(), "test3")