import numpy as np
from unittest import TestCase
from dcp.dcp_12 import Dcp12


class TestDcp12(TestCase):
    def test_calculate_smallest_sorting_window_known(self):
        test_array = [1, 5, 3, 7, 6]
        dcp12 = Dcp12(test_array)
        window = dcp12.calculate_smallest_sorting_window()
        print(test_array)
        print(window)
        self.assertTrue(True, "Asserting True is True")

    def test_calculate_smallest_sorting_window(self):
        test_array = np.random.randint(1, 11, np.random.randint(10))
        dcp12 = Dcp12(test_array)
        window = dcp12.calculate_smallest_sorting_window()
        print(test_array)
        print(window)
        self.assertTrue(True, "Asserting True is True")
