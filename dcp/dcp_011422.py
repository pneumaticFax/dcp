# Good morning! Here's your coding interview problem for today.
# This problem was asked by Facebook.
# Given a string of round, curly, and square open and closing brackets,
# return whether the brackets are balanced (well-formed).
# For example, given the string "([])[]({})", you should return true.
# Given the string "([)]" or "((()", you should return false.

class Dcp011422:

    def __init__(self, input_string):
        self.open_brackets = {"(", "[", "{"}
        self.closed_brackets = {")", "]", "}"}
        self.letter_dict = {
            "{": "}",
            "(": ")",
            "[": "]"
        }
        self.input_string = input_string

    def determine_string_balance(self):
        stack = []

        if len(self.input_string) == 0:
            return False
        for letter in self.input_string:
            if len(stack) == 0 and letter in self.closed_brackets:
                return False

            if letter in self.open_brackets:
                stack.append(letter)

            if letter in self.closed_brackets:
                top_stack_letter = stack.pop()

                if self.letter_dict.get(top_stack_letter) != letter:
                    return False

        if len(stack) != 0:
            return False

        return True





