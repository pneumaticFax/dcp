# Given an array of integers nums which is sorted in ascending order,
# and an integer target, write a function to search target in nums.
# If target exists, then return its index. Otherwise, return -1.
#
# You must write an algorithm with O(log n) runtime complexity.


class BinarySearch:
    def __init__(self, nums, target):
        self.nums = nums
        self.target = target

    def solution(self):
        nums = self.nums
        target = self.target

        left = 0
        right = len(nums) - 1
        mid = int((right + left) / 2)

        while right >= left:
            if target == nums[mid]:
                return mid
            if target < nums[mid]:
                right = mid - 1
                mid = int((right + left) / 2)
            if target > nums[mid]:
                left = mid + 1
                mid = int((right + left) / 2)
        return -1


