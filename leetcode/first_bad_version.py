class FirstBadVersion(object):

    def __init__(self):
        self.arr_n = ['good', 'good', 'good', 'good', 'good', 'bad', 'bad', 'bad']
        self.n = len(self.arr_n)

    def isBadVersion(self, index):
        if self.arr_n[index] == 'bad':
            return True
        return False

    def solution(self):
        n = self.n
        # # [good, good, good, good, good, bad, bad, bad]
        # # 0      2     3     4     5     6    7    8
        # #
        # split half point is good
        # split half again until find bad
        # last good start
        # bad stop
        # from last good start check until bad
        #
        # or better probably keep
        # cutting in half until you find 2 bads in a row, or maybe next to each other
        #
        left = 0
        right = n - 1
        mid = int((left+right)/2)

        if not self.isBadVersion(right):
            return -1

        while left < right:
            if self.isBadVersion(mid):
                if self.isBadVersion(mid-1) is False:
                    return mid
            right = mid - 1
            mid = int((left+right)/2)
        return -1



