# Given an array of distinct integers candidates and a target integer target,
# return a list of all unique combinations of candidates where the chosen numbers sum to target.
# You may return the combinations in any order.
#
# The same number may be chosen from candidates an unlimited number of times.
# Two combinations are unique if the frequency of at least one of the chosen numbers is different.
#
# It is guaranteed that the number of unique combinations that sum up to
# target is less than 150 combinations for the given input.
from collections import defaultdict


class CombinationSum:
    def __init__(self, candidates, target):
        self.candidates = candidates
        self.target = target

    # recursive function for finding solutions
    def backtracking(self, candidates, target, start_index, current_list, result):
        # candidates: our sorted list of candidates
        # target: our target from parent
        # start_index: our current position in the list
        # current_list: our 'in progress' solution
        # result: list of valid solutions 'in progress'

        # if target == 0 return the current list solution was found
        # else loop through the list, check current candidate agaisnt target, if less add it to current solution
        #   pass in the new start index ( could be the same ) into recursive call
        #   subtract candidate from target and pass into recursive call

        if target == 0:
            result.append(current_list)
            return

        for i in range(start_index, len(candidates)):
            current_candidate = candidates[i]
            if current_candidate <= target:
                self.backtracking(candidates, target - current_candidate, i, current_list + [current_candidate], result)
            else:
                break

    def solve(self):
        candidates = self.candidates
        target = self.target
        result = []
        candidates.sort()
        # Backtrack(x)
        #   if x is not a solution
        #       return false
        #   if x is a new solution
        #       add to list of solutions
        #   backtrack(expand x)

        self.backtracking(candidates, target, 0, [], result)

        return result
