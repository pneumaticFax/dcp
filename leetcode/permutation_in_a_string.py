# Given two strings s1 and s2, return true if s2 contains a permutation of s1, or false otherwise.
#
# In other words, return true if one of s1's permutations is the substring of s2.

# Example 1:
#
# Input: s1 = "ab", s2 = "eidbaooo"
# Output: true
# Explanation: s2 contains one permutation of s1 ("ba").
#
# Example 2:
#
# Output: false
#
# Input: s1 = "ab", s2 = "eidboaoo"
from collections import Counter


class PermutationInString:
    def __init__(self, s1, s2):
        self.s1 = s1
        self.s2 = s2

    def solve(self):
        s1 = self.s1
        s2 = self.s2

        set_s1 = Counter(s1)
        for i, element in enumerate(s2):
            # cycle through s2 until we find a letter that's in s1
            if element in set_s1:
                # take a copy of set_s1 to modify
                test_set_s1 = set_s1.copy()
                # remove that instance for the letter from the s1 counter
                # test_set_s1[element] -= 1
                # check the next len(s1) letters of s2
                for j in range(len(s1)):
                    if i+j >= len(s2):
                        return False
                    elif test_set_s1[s2[i+j]]:
                        test_set_s1[s2[i+j]] -= 1
                    else:
                        break
                if not any(test_set_s1.values()):
                    return True
        return False

    # def solve_faster(self):
    #     s1 = self.s1
    #     s2 = self.s2
    #
    #     set_s1 = Counter(s1)
    #     # take a copy of set_s1 to modify
    #     test_set_s1 = set_s1.copy()
    #     for i, element in enumerate(s2):
    #         # if element is in s1 with a value of more than 0
    #         if test_set_s1[element]:
    #             # remove that instance for the letter from the s1 counter
    #             test_set_s1[element] -= 1
    #         else:
    #             test_set_s1 = set_s1.copy()
    #         if not any(test_set_s1.values()):
    #             return True
    #     return False

    def solve_trick(self):
        s1 = self.s1
        s2 = self.s2

        val_s1 = []
        val_s2 = []

        len_s1 = len(s1)
        for i, char in enumerate(s1):
            val_s1.append(ord(char))

        for i, char in enumerate(s2):
            val_s2.append(ord(char))

        for i in range(len(s2)-len(s1)+1):
            next_substring_sum = sum(val_s2[i:i + len_s1])
            if next_substring_sum == sum(val_s1):
                if Counter(val_s1) == Counter(val_s2[i:i + len_s1]):
                    return True
        return False

    def solve_trick2(self):
        s1 = self.s1
        s2 = self.s2

        len_s1 = len(s1)
        counter_s1 = Counter(s1)

        for i in range(len(s2)-len(s1)+1):
            if counter_s1 == Counter(s2[i:i + len_s1]):
                return True
        return False
