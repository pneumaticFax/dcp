# Given an array of integers nums and an integer k, return the number of unique k-diff pairs in the array.
#
# A k-diff pair is an integer pair (nums[i], nums[j]), where the following are true:
#
#     0 <= i < j < nums.length
#     |nums[i] - nums[j]| == k
#
# Notice that |val| denotes the absolute value of val.
#
# Example 1:
#
# Input: nums = [3,1,4,1,5], k = 2
# Output: 2
# Explanation: There are two 2-diff pairs in the array, (1, 3) and (3, 5).
# Although we have two 1s in the input, we should only return the number of unique pairs.
#
# Example 2:
#
# Input: nums = [1,2,3,4,5], k = 1
# Output: 4
# Explanation: There are four 1-diff pairs in the array, (1, 2), (2, 3), (3, 4) and (4, 5).
#
# Example 3:
#
# Input: nums = [1,3,1,5,4], k = 0
# Output: 1
# Explanation: There is one 0-diff pair in the array, (1, 1).
from collections import defaultdict, Counter


class KDiffPairsInArray:

    def __init__(self, nums, k):
        self.nums = nums
        self.k = k

    def solution(self):
        nums = self.nums
        k = self.k

        # pair = set()
        pairs = set()

        for i, val in enumerate(nums):
            for j, other_val in enumerate(nums):
                if i != j:
                    if abs(val - other_val) == k:
                        if val < other_val:
                            pair = (val, other_val)
                        else:
                            pair = (other_val, val)
                        pairs.add(pair)

        return len(pairs)

    def solution_faster(self):
        nums = self.nums
        k = self.k

        # hashtable/hashmap/dict (O(1) lookup) nums where key is just the value of the number, value can be the same,
        # then for each value of nums look at index k - nums, if found increment a counter
        # data = hashmap
        data = defaultdict(int)
        pairs = set()

        for num in nums:
            data[num] += 1

        # if the k value is 0 then key and compliment must be equal and only one instance of (key,comp) can be counted
        if k == 0:
            duplicate_count = 0
            for key in data.keys():
                if key in data and data[key] > 1:
                    duplicate_count += 1
                    data[key] = 0
            return duplicate_count

        for key in data.keys():
            compliment1 = key - k
            compliment2 = key + k

            if compliment1 in data:
                if key < compliment1:
                    pair = (key, compliment1)
                    pairs.add(pair)
                elif key > compliment1:
                    pair = (compliment1, key)
                    pairs.add(pair)

            if compliment2 in data:
                if key < compliment2:
                    pair = (key, compliment2)
                    pairs.add(pair)
                elif key > compliment2:
                    pair = (compliment2, key)
                    pairs.add(pair)

        return len(pairs)

    def solution_faster_shorter(self):
        nums = self.nums
        k = self.k

        count = 0
        d = {}
        for i in nums:
            if i in d:
                d[i] += 1
            else:
                d[i] = 1

        if k == 0:
            for i in set(nums):
                if d[i] >= 2:
                    count += 1

        else:
            nums = set(nums)
            for i in nums:
                a = i - k
                if a in d:
                    count += 1
        return count




