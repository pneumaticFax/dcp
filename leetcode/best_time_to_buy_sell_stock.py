# You are given an array prices where prices[i] is the price of a given stock on the ith day.
#
# You want to maximize your profit by choosing a single day to buy one stock and
# choosing a different day in the future to sell that stock.
#
# Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.

class BestTimeToBuySellStock:
    def __init__(self, prices):
        self.prices = prices

    def max_profit(self):
        prices = self.prices

        min_seen = float("inf")
        max_profit = float("-inf")

        for i in range(0, len(prices)):

            min_seen = min(min_seen, prices[i])
            max_profit = max(max_profit, prices[i]-min_seen)

        return max(0, max_profit)
