# Given an array of integers nums and an integer k,
# return the total number of continuous subarrays whose sum equals to k.
#
# Input: nums = [1,1,1], k = 2
# Output: 2
#
# Input: nums = [1,2,3], k = 3
# Output: 2
from collections import defaultdict


class SubarraySumEqualsK:
    def __init__(self, nums, k):
        self.nums = nums
        self.k = k

    def solution(self):
        nums = self.nums
        k = self.k
        running_sum = 0
        count = 0
        # pick a item, then walk forward until we either == k or >k
        # if ==k add to counter if >k increment to next step
        # runs in O(n^2)
        for i, element in enumerate(nums):
            running_sum = element
            if running_sum == k:
                count += 1

            for j in range(i+1, len(nums)):  #check this for array end point validity
                running_sum += nums[j]
                if running_sum == k:
                    count += 1

        return count

    def solution_faster(self):
        nums = self.nums
        k = self.k
        running_sum = 0
        count = 0
        # look for a trick to determine if we equal k.
            # can dump nums into a hashmap/table
            # for each key in the map we can look at k - key gives us a sum we need.
            # for each key from key to k-key
        nums_dict = defaultdict(int)
        for i, element in enumerate(nums):
            # start/add to  a running sum
            running_sum += element

            # current running sum is our target?
            if running_sum == k:
                # add our to our hit count
                count += 1

            # if running sum - k (the compliment to running sum to make k, as in [running_sum + compliment = k]),
            # is in our dictionary already, add the number of times its come up to our hit counter
            if running_sum - k in nums_dict:
                count += nums_dict[running_sum - k]

            # put the running sum into the dictionary or add to its frequency
            # the next running sum might use this to make k
            nums_dict[running_sum] += 1
        return count





