# Given two strings s and p, return an array of all the start indices of p's anagrams in s.
# You may return the answer in any order.
#
# An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
# # typically using all the original letters exactly once.
# Input: s = "cbaebabacd", p = "abc"
# Output: [0,6]
# Explanation:
# The substring with start index = 0 is "cba", which is an anagram of "abc".
# The substring with start index = 6 is "bac", which is an anagram of "abc".
# Input: s = "abab", p = "ab"
# Output: [0,1,2]
# Explanation:
# The substring with start index = 0 is "ab", which is an anagram of "ab".
# The substring with start index = 1 is "ba", which is an anagram of "ab".
# The substring with start index = 2 is "ab", which is an anagram of "ab".
from collections import defaultdict, Counter


class AnagramsInAString:
    def __init__(self, s, p):
        self.s = s
        self.p = p

    def find_anagrams_better(self):
        s = self.s
        p = self.p

        hash_map = defaultdict(int)
        new_map = hash_map

        len_p = len(p)
        len_s = len(s)
        ret_vals = []
        # [a,b,c,d] len = 4

        if len_p > len_s:
            return ret_vals
        for i in range(len_p):
            hash_map[p[i]] += 1

        for i in range(len_p - 1):
            if s[i] in hash_map:
                hash_map[s[i]] -= 1

        for i in range(-1, len_s - len_p + 1):
            if i > -1 and s[i] in hash_map:
                hash_map[s[i]] += 1
            if i + len_p < len_s and s[i+len_p] in hash_map:
                hash_map[s[i+len_p]] -= 1
            if all(v == 0 for v in hash_map.values()):
                ret_vals.append(i+1)

        return ret_vals

    def find_anagrams(self):
        s = self.s
        p = self.p

        set_s = set()
        set_p = set()

        len_p = len(p)
        len_s = len(s)
        sum_slice_s = 0
        sum_p = 0
        sum_s = 0
        ret_vals = []
        # [a,b,c,d] len = 4

        for i in range(0, len_p):
            set_p.add(p[i])
            sum_p += ord(p[i])
        # [a,b,c,d] len = 4; [a,b] len = 2
        for i in range(0, len_s - len_p + 1):
            slice_s = s[i:i + len_p]
            for j in range(0, len(slice_s)):
                set_s.add(slice_s[j])
                sum_s += ord(slice_s[j])
            if set_s == set_p and sum_s == sum_p:
                ret_vals.append(i)
            set_s.clear()
            sum_s = 0
        return ret_vals
