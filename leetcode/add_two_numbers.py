# You are given two non-empty linked lists representing two non-negative integers.
# The digits are stored in reverse order, and each of their nodes contains a single digit.
# Add the two numbers and return the sum as a linked list.
#
# You may assume the two numbers do not contain any leading zero, except the number 0 itself.
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class AddTwoNumbers:
    def __init__(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        self.l1 = l1
        self.l2 = l2

    def solution(self):
        l1 = self.l1
        l2 = self.l2

        l1 = ListNode(2)

        head = ListNode()
        current_node = head
        # 2-4-3  -> 342
        # 5-6-4  -> 465
        # 7-10-7 -> 7-0-8 -> 807
        carry = 0
        total = 0

        while l1 or l2 or carry > 0:
            if l1:
                total += l1.val
                l1 = l1.next

            if l2:
                total += l2.val
                l2 = l2.next

            if total >= 10:
                carry = 1
                total -= 10

            current_node.val = total
            total = 0

            if l1 or l2 or carry > 0:
                next_node = ListNode()
                current_node.next = next_node
                current_node = next_node

        return head.next




