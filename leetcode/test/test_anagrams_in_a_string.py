import unittest
from leetcode import sliding_window_anagrams_in_a_string


class MyTestCase(unittest.TestCase):

    def test_something(self):
        s = "cbaebabacd"
        p = "abc"
        test_case = anagrams_in_a_string.AnagramsInAString(s, p)
        resolved = test_case.find_anagrams_better()
        print(resolved)
        # self.assertEqual(True, False)  # add assertion here


    def test_something2(self):
        s = "abab"
        p = "ab"

        test_case = anagrams_in_a_string.AnagramsInAString(s, p)
        resolved = test_case.find_anagrams_better()
        print(resolved)
        # self.assertEqual(True, False)  # add assertion here

    def test_something3(self):
        s = "bpaa"
        p = "aa"

        test_case = anagrams_in_a_string.AnagramsInAString(s, p)
        resolved = test_case.find_anagrams_better()
        print(resolved)
        # self.assertEqual(True, False)  # add assertion here
    def test_something4(self):
        s = "af"
        p = "be"

        test_case = anagrams_in_a_string.AnagramsInAString(s, p)
        resolved = test_case.find_anagrams_better()
        print(resolved)
        # self.assertEqual(True, False)  # add assertion here

if __name__ == '__main__':
    unittest.main()
