import unittest
from leetcode import add_digits


class MyTestCase(unittest.TestCase):
    def test_something(self):
        test_case = add_digits.AddDigits(38)
        result = 2
        self.assertEqual(result, test_case.solution())  # add assertion here

    def test_something2(self):
        test_case = add_digits.AddDigits(19)
        result = 1
        self.assertEqual(result, test_case.solution())  # add assertion here

    def test_something3(self):
        test_case = add_digits.AddDigits(19)
        result = 1
        self.assertEqual(result, test_case.solution_faster())  # add assertion here

    def test_something4(self):
        test_case = add_digits.AddDigits(18)
        result = 9
        self.assertEqual(result, test_case.solution_faster())  # add assertion here


if __name__ == '__main__':
    unittest.main()
