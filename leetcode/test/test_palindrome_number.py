import unittest
from leetcode import palindrome_number

class MyTestCase(unittest.TestCase):
    def test_something(self):
        test_case = palindrome_number.PalindromeNumber(121)
        self.assertEqual(True, test_case.solution_const_space())  # add assertion here


if __name__ == '__main__':
    unittest.main()
