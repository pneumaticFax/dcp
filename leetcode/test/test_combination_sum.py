import unittest
from leetcode import backtracking_combination_sum


class MyTestCase(unittest.TestCase):
    def test_something(self):
        candidates = [7,3,2,6]
        target = 7
        test_case = backtracking_combination_sum.CombinationSum(candidates, target)
        expected = [[2,2,3],[7]]
        result = test_case.solve()

        self.assertListEqual(expected, result)  # add assertion here


if __name__ == '__main__':
    unittest.main()
