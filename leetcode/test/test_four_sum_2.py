import unittest
from leetcode import four_sum_2

class MyTestCase(unittest.TestCase):
    def test_something(self):

        nums1 = [1, 2]
        nums2 = [-2, -1]
        nums3 = [-1, 2]
        nums4 = [0, 2]
        test_case = four_sum_2.FourSum2(nums1, nums2, nums3, nums4)
        print(test_case.solution_better())
        self.assertEqual(2, test_case.solution())  # add assertion here


if __name__ == '__main__':
    unittest.main()
