import unittest
from leetcode import permutation_in_a_string


class MyTestCase(unittest.TestCase):

    def test_something(self):
        s1 = "ab"
        s2 = "eidbaooo"
        expected = True

        test_case = permutation_in_a_string.PermutationInString(s1, s2)
        res = test_case.solve_trick2()
        self.assertEqual(expected, res)  # add assertion here

    def test_something2(self):
        s1 = "ab"
        s2 = "eidboaoo"
        expected = False

        test_case = permutation_in_a_string.PermutationInString(s1, s2)
        res = test_case.solve_trick2()
        self.assertEqual(expected, res)  # add assertion here

    def test_something3(self):
        s1 = "hello"
        s2 = "ooolleoooleh"
        expected = False

        test_case = permutation_in_a_string.PermutationInString(s1, s2)
        res = test_case.solve_trick2()
        self.assertEqual(expected, res)  # add assertion here

    def test_something4(self):
        s1 = "adc"
        s2 = "dcda"
        expected = True

        test_case = permutation_in_a_string.PermutationInString(s1, s2)
        res = test_case.solve_trick2()
        self.assertEqual(expected, res)  # add assertion here

    def test_something4(self):
        s1 = "abc"
        s2 = "ccccbbbbaaaa"
        expected = False

        test_case = permutation_in_a_string.PermutationInString(s1, s2)
        res = test_case.solve_trick2()
        self.assertEqual(expected, res)  # add assertion here


if __name__ == '__main__':
    unittest.main()
