import unittest
from leetcode import best_time_to_buy_sell_stock


class MyTestCase(unittest.TestCase):
    def test_something(self):
        prices = [7, 1, 5, 3, 6, 4]
        test_case = best_time_to_buy_sell_stock.BestTimeToBuySellStock(prices)
        max_val = test_case.max_profit()
        print(max_val)
        self.assertEqual(5, max_val)

    def test_something2(self):
        prices = [7,6,4,3,1]
        test_case = best_time_to_buy_sell_stock.BestTimeToBuySellStock(prices)
        max_val = test_case.max_profit()
        print(max_val)
        self.assertEqual(0, max_val)

    def test_something3(self):
        prices = [2,1]
        test_case = best_time_to_buy_sell_stock.BestTimeToBuySellStock(prices)
        max_val = test_case.max_profit()
        print(max_val)
        self.assertEqual(0, max_val)

    def test_something4(self):
        prices = [1,4,2]
        test_case = best_time_to_buy_sell_stock.BestTimeToBuySellStock(prices)
        max_val = test_case.max_profit()
        print(max_val)
        self.assertEqual(3, max_val)

    def test_something5(self):
        prices = [3,2,6,5,0,3]
        test_case = best_time_to_buy_sell_stock.BestTimeToBuySellStock(prices)
        max_val = test_case.max_profit()
        print(max_val)
        self.assertEqual(4, max_val)


if __name__ == '__main__':
    unittest.main()
