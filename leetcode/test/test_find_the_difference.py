import unittest
from leetcode import find_the_difference


class MyTestCase(unittest.TestCase):
    def test_something(self):
        s = "abcd"
        t = "abcde"
        test_case = find_the_difference.FindTheDifference(s,t)
        solution = test_case.solution()
        print(solution)
        self.assertEqual('e', solution)  # add assertion here

    def test_something2(self):
        s = ""
        t = "y"
        test_case = find_the_difference.FindTheDifference(s,t)
        solution = test_case.solution()
        print(solution)
        self.assertEqual('y', solution)  # add assertion here

    def test_something3(self):
        s = "abcd"
        t = "abcde"
        test_case = find_the_difference.FindTheDifference(s,t)
        solution = test_case.solution_ascii()
        print(solution)
        self.assertEqual('e', solution)  # add assertion here

    def test_something4(self):
        s = ""
        t = "y"
        test_case = find_the_difference.FindTheDifference(s,t)
        solution = test_case.solution_ascii()
        print(solution)
        self.assertEqual('y', solution)  # add assertion here



if __name__ == '__main__':
    unittest.main()
