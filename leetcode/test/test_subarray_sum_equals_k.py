import unittest
from leetcode import subarray_sum_equals_k


class MyTestCase(unittest.TestCase):
    def test_something(self):
        nums = [1, 1, 1]
        k = 2
        output = 2
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something2(self):
        nums = [1, 2, 3]
        k = 3
        output = 2
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something3(self):
        nums = [1, -1, 0]
        k = 0
        output = 3
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something4(self):
        nums = [0, 0]
        k = 0
        output = 3
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something5(self):
        nums = [28, 54, 7, -70, 22, 65, -6]
        k = 100
        output = 1
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something0(self):
        nums = [1, 1, 1]
        k = 2
        output = 2
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution_faster()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something02(self):
        nums = [1, 2, 3]
        k = 3
        output = 2
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution_faster()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something03(self):
        nums = [1, -1, 0]
        k = 0
        output = 3
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution_faster()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something04(self):
        nums = [0, 0]
        k = 0
        output = 3
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution_faster()
        self.assertEqual(output, test_case)  # add assertion here

    def test_something05(self):
        nums = [28, 54, 7, -70, 22, 65, -6]
        k = 100
        output = 1
        test_case = subarray_sum_equals_k.SubarraySumEqualsK(nums, k).solution_faster()
        self.assertEqual(output, test_case)  # add assertion here
if __name__ == '__main__':
    unittest.main()
