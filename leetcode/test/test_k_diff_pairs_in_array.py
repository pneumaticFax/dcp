import unittest
from leetcode import k_diff_pairs_in_array


class MyTestCase(unittest.TestCase):

    def test_something(self):
        nums = [3, 1, 4, 1, 5]
        k = 2
        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        solution = 2
        test_answer = test_case.solution()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something2(self):
        nums = [1,3,1,5,4]
        k = 0
        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        solution = 1
        test_answer = test_case.solution()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something21(self):
        nums = [1, 2, 4, 4, 3, 3, 0, 9, 2, 3]
        k = 3
        solution = 2

        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        test_answer = test_case.solution()
        self.assertEqual(solution, test_answer)  # add assertion here


    def test_something3(self):
        nums = [3, 1, 4, 1, 5]
        k = 2
        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        solution = 2
        test_answer = test_case.solution_faster()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something4(self):
        nums = [1,3,1,5,4]
        k = 0
        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        solution = 1
        test_answer = test_case.solution_faster()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something5(self):
        nums = [1, 2, 4, 4, 3, 3, 0, 9, 2, 3]
        k = 3
        solution = 2

        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        test_answer = test_case.solution_faster()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something6(self):
        nums = [3, 1, 4, 1, 5]
        k = 2
        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        solution = 2
        test_answer = test_case.solution_faster_shorter()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something7(self):
        nums = [1, 3, 1, 5, 4]
        k = 0
        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        solution = 1
        test_answer = test_case.solution_faster_shorter()
        self.assertEqual(solution, test_answer)  # add assertion here

    def test_something8(self):
        nums = [1, 2, 4, 4, 3, 3, 0, 9, 2, 3]
        k = 3
        solution = 2

        test_case = k_diff_pairs_in_array.KDiffPairsInArray(nums, k)
        test_answer = test_case.solution_faster_shorter()
        self.assertEqual(solution, test_answer)  # add assertion here


if __name__ == '__main__':
    unittest.main()
