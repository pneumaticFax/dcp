import unittest
from leetcode import binary_search

class MyTestCase(unittest.TestCase):
    def test_something(self):
        nums = [-1, 0, 3, 5, 9, 12]
        target = 2
        test_case = binary_search.BinarySearch(nums, target)
        self.assertEqual(-1, test_case.solution())  # add assertion here



if __name__ == '__main__':
    unittest.main()
