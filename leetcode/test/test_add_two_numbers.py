import unittest
from leetcode import add_two_numbers



class MyTestCase(unittest.TestCase):
    def test_something(self):
        l1 = add_two_numbers.ListNode()
        l1 = [2, 4, 3]
        l2 = [5, 6, 4]
        test_case = add_two_numbers.AddTwoNumbers(l1, l2)
        self.assertEqual([7, 0, 8], test_case.solution())  # add assertion here


if __name__ == '__main__':
    unittest.main()
