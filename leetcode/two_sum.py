# # Given an array of integers nums and an integer target,
# # return indices of the two numbers such that they add up to target.
# #
# # You may assume that each input would have exactly one solution,
# # and you may not use the same element twice.
# #
# # You can return the answer in any order.
# Example 1:
#
# Input: nums = [2,7,11,15], target = 9
# Output: [0,1]
# Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
#
# Example 2:
#
# Input: nums = [3,2,4], target = 6
# Output: [1,2]
#
# Example 3:
#
# Input: nums = [3,3], target = 6
# Output: [0,1]
# https://leetcode.com/problems/two-sum/


class TwoSum:
    def __init__(self, nums, target):
        self.nums = nums
        self.target = target

    def solve(self):
        nums = self.nums
        target = self.target
        result = []
        for i in range(len(nums)):
            for j in range(len(nums)):
                if i != j and nums[i] + nums[j] == target:
                    if i < j:
                        result.append(i)
                        result.append(j)
                    else:
                        result.append(j)
                        result.append(i)
        return result

    def solve_better(self):
        nums = self.nums
        target = self.target
        result = []
        num_dict = {}
        for i, element in enumerate(nums):
            num_dict.update({element: i})

        if len(num_dict) != len(nums):
            # check if our target/2 is duplicated in the array
            if target / 2 in num_dict.keys():
                for i in range(0, num_dict.get(target / 2)):
                    if nums[i] == target / 2:
                        result.append(i)
            result.append(num_dict.get(target / 2))
            if len(result) == 2:
                return result
            else:
                result.clear()

        for k in num_dict.keys():
            if target - k in num_dict.keys() and target - k != k:
                result.append(num_dict.get(k))
                result.append(num_dict.get(target - k))
                return result

        return result
