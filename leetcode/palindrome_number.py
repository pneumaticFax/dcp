class PalindromeNumber:
    def __init__(self, x):
        self.x = x

    def solution(self):
        x = self.x

        str_x = str(x)
        i = 0
        while i < (len(str_x)-1-i):
            if str_x[i] != str_x[len(str_x)-1 - i]:
                return False
            i += 1

        return True

    def solution_const_space(self):
        x = self.x
        if x < 0:
            return False

        backwards_x = 0
        test_x = x
        while test_x != 0:
            # 121 % 10 = 1
            # 0 + 1 = 1
            # 12 %10 = 2
            # 10+2 = 12
            # 1 % 10 = 1
            # 12*10 =120+1 = 121

            backwards_x = (backwards_x * 10) + (test_x % 10)
            test_x = test_x/10
            test_x = int(test_x)
        if backwards_x == x:
            return True

        return False
