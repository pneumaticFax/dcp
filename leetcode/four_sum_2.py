# Given four integer arrays nums1, nums2, nums3, and nums4
# all of length n, return the number of tuples (i, j, k, l) such that:
#
#     0 <= i, j, k, l < n
#     nums1[i] + nums2[j] + nums3[k] + nums4[l] == 0
#
# # Example
# 1:
#
# Input: nums1 = [1, 2], nums2 = [-2, -1], nums3 = [-1, 2], nums4 = [0, 2]
# Output: 2
# Explanation:
# The two tuples are:
# 1.(0, 0, 0, 1) -> nums1[0] + nums2[0] + nums3[0] + nums4[1] = 1 + (-2) + (-1) + 2 = 0
# 2.(1, 1, 0, 0) -> nums1[1] + nums2[1] + nums3[0] + nums4[0] = 2 + (-1) + (-1) + 0 = 0
#
# Example
# 2:
#
# Input: nums1 = [0], nums2 = [0], nums3 = [0], nums4 = [0]
# Output: 1
from collections import defaultdict


class FourSum2:
    def __init__(self, nums1, nums2, nums3, nums4):
        self.nums1 = nums1
        self.nums2 = nums2
        self.nums3 = nums3
        self.nums4 = nums4

    def solution(self):
        nums1 = self.nums1
        nums2 = self.nums2
        nums3 = self.nums3
        nums4 = self.nums4

        ret_val = 0

        # strategy, add 1 number from each array of same length to make 0
        # brute force: iterate the whole array one step at a time, check if it equals 0

        for i in range(len(nums1)):
            for j in range(len(nums2)):
                for k in range(len(nums3)):
                    for l in range(len(nums4)):
                        if nums1[i]+nums2[j]+nums3[k]+nums4[l] == 0:
                            ret_val += 1

        return ret_val

    def solution_better(self):
        nums1 = self.nums1
        nums2 = self.nums2
        nums3 = self.nums3
        nums4 = self.nums4

        dict1234 = defaultdict(int)

        ret_val = 0
        arr_len = len(nums1)

        # add values in nums1, nums2 into a dictionary key:value where key is the sum and value is the number of times
        # the sum has come up, we will use default dict to make it easier to add default value if it doesnt exist
        # we have a match from the n1+n2

        #to check for a hit where n1+n2+n3+n4 = 0 or n1 + n2 = -(n3 + n4)
        hit = 0
        for i in range(arr_len):
            for j in range(arr_len):
                # add each iteration of nums1 to nums2 (O(n2))
                dict1234[nums1[i]+nums2[j]] += 1

        for i in range(arr_len):
            for j in range(arr_len):
                hit = dict1234[-nums3[i]-nums4[j]]
                ret_val += hit

        return ret_val