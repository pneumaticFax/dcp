# Given an integer num, repeatedly add all its digits until the result has only one digit, and return it.
# Input: num = 38
# Output: 2
# Explanation: The process is
# 38 --> 3 + 8 --> 11
# 11 --> 1 + 1 --> 2
# Since 2 has only one digit, return it.

class AddDigits:
    def __init__(self, num):
        self.num = num

    def solution(self):
        num = self.num

        # split our number into digits, lets use string then dump into list or something
        # - could also do with modulo probably faster
        # [3][8][11][1][1][2]
        # [0][0][11]
        # 0 0 0 1 1
        #if bigger than 10 set to 0 and then start again
        list_nums = []

        for char in str(num):
            list_nums.append(int(char))

        digits_sum = 0
        for i, num in enumerate(list_nums):
            digits_sum += num
            list_nums[i] = 0
            if digits_sum >= 10:     # split and do it again
                for char in str(digits_sum):
                    list_nums.append(int(char))
                digits_sum = 0

        return digits_sum

    def solution_faster(self):  #digit root approach
        num = self.num
        if num == 0:
            return 0    # value is zero just return also gets in the way of next check
        if num % 9 == 0:
            return 9    # take into account when numbers = 9 (81,72,63,54,45,363,27,18,9)
        else:
            return num % 9  # last case the remainder will be the sum (radix, digital root,

